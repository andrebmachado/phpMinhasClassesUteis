﻿<?php

class fsUtils{
    private $pathDir = "./stdFolder/";
    private $pathSubDir = "./stdFolder/stdFolderSub";
    private $fileExtension  = "*.txt";
    private $nFiles = array();
    private $fileNames;
	private $logs;
	private $limitLogs;
    
    public function __construct($params=[]){
        foreach ($params as $key => $value) {
           $this->{$key} = $value;
        }
		$this->logs .= "====================== ".date('d/m/Y H:m:s')."Iniciando classe".__CLASS__." ======================\n";
        $this->loadFolder();				
    }
	public function __destruct() {	   
	    $this->logs .= "===================================================================================================\n\n";
		$this->log();
	}	
    private function loadFolder(){		
        $cont=0;
		$pathTemp = $this->pathDir."/".$this->fileExtension;		
        foreach (glob($pathTemp) as $arquivo){
            $this->nFiles[$cont] = $arquivo;                        
            $arr = explode($this->pathDir, $arquivo);
            $this->fileNames[$cont] = $arr[1];
            $cont++;            
        }
		$this->logs .= 'Numero de arquivos: '.sizeof(glob($pathTemp))."\n";
    }	
	private function log($sizeLimit=5){
		$dirLog  = __CLASS__."Logs".DIRECTORY_SEPARATOR;
		$fileLog = __CLASS__.'.log';
		$dirFileLog = $dirLog.$fileLog;
		$newFileName = $dirLog.(count(glob($dirLog."*.log"))+1)."_".$fileLog;
		$numFiles = count(glob($dirLog."*.log"));
		$fileLogs = glob($dirLog."*.log");
		$str = '';
		//print_r($fileLogs);exit;
		//Verifica o tamanho do arquivo se 		
		(file_exists($dirFileLog))? $currentSizeFile = number_format(filesize($dirFileLog)/1024,2): $currentSizeFile=0;
		//se o log principal for maior ele renomeia com o prefixo numerico referente o numero de logs na pasta
		if($sizeLimit<$currentSizeFile){
			rename($dirFileLog,$newFileName);
			$this->logs .= 'Arquivo de log renomeado : '.$fileLog."->".$newFileName."\n";
		}		
		
		if($this->limitLogs<=$numFiles){			
			for($i=0;$i<$this->limitLogs;$i++){
				if(file_exists($fileLogs[$i])){
					//unlink($this->nFiles[$i]);
					$str .=  $fileLogs[$i]."\n";
				}
			}
			$this->logs .= "Logs da classe excluidos:\n";
			$this->logs .= $str."\n\n\n";
		}		
		$this->saveToFile($dirFileLog,$this->logs,false,'log');
	}
	/*
		@filename = nome do arquivo que sera gerado se nao informar gera 
		@contentFile = conteudo que sera salvo no arquivo 
		@prefix=true gera um prefixo numerico para cada arquivo conforme numero de arquivos na pasta
		@fileType=new => gera um novo arquivo
		@fileType=log => salva no mesmo arquivo 
	*/
	public function saveToFile($filename="",$contentFile,$prefix=false,$fileType='new'){
			$numFiles = str_pad(count($this->nFiles), 3, "0", STR_PAD_LEFT);
			($prefix)? $filename = dirname($filename).'/'.$numFiles++.'_'.basename($filename) : $prefix=false;
			($fileType=='log')? $fopen='a+' : $fopen='w';
			($filename==""   )? $filename = __CLASS__.date('Ymd').".log" : $filename;
		
			if(!file_exists(dirname($filename))){
				$this->logs .= "Criando pasta--> ".dirname($filename)."\n";	
				mkdir(dirname($filename), 0755, true);
				chmod(dirname($filename), 0755);				
			}
			try {
				if(($f=fopen($filename, $fopen))){
					if(fwrite($f, $contentFile,strlen($contentFile))){
						$this->logs .= "Gerando arquivo--> ".$filename."\n";	
					}
				}else{
					$this->logs .= "Falha ao gerar o arquivo!";
					throw new Exception("Falha ao gerar o arquivo!");
					$p = basename($filename);
				}
				fclose($f);
			} catch (Exception $e) {				
				$logMsg = "Exception:".$e->getMessage()." - File: ".$e->getFile()." Linha: ".$e->getLine()."\n";
				$this->logs .= $logMsg;
				return false;
				fclose($f);
			} 
		
	}	
	public function delDepoisXFiles($num){
		$this->limitLogs = $num;
		$numFiles = count($this->nFiles);
		$str = '';
		if($num<=$numFiles){
			for($i=0;$i<$num;$i++){
				if(file_exists($this->nFiles[$i])){
					unlink($this->nFiles[$i]);
					$str .=  $this->nFiles[$i]."\n";
				}
			}
			$this->logs .= "Logs excluidos:\n";
			$this->logs .= $str;
		}
	}	
    public function getListFolder(){
        return $this->filesRetorno;
    }
    public function getListFiles(){
        return $this->fileNames;
    }   
    private function moveFile($arquivos){        
        foreach ($arquivos as $file){
            rename($file, $this->retornosLidos.$file);
        }        
    }
	
	
	
}

//$demo =  	new fsUtils(array(
//			   "pathDir"=>"./retornoLogs/",
//			   "fileExtension"=>"*.LOG"
//			)); 

//$demo->delDepoisXFiles(5);