﻿<?php

//$valor 		= "R$10,20";
//
//$vencimento = "10/02/2009";
//$f 	   		= fopen("data.txt","a+",0);
//$linha 		= $valor.":".$vencimento.":\n";
//fwrite($f,$linha,strlen($linha));
//fclose($f);
//	

class MyDestructableClass {
   function __construct() {
       print "In constructor\n";
       $this->name = "MyDestructableClass";
   }

   function __destruct() {
       print "Destroying " . $this->name . "\n";
   }
}

$obj = new MyDestructableClass();