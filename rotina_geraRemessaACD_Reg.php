<?php
//if($_GET['argv']){ $argv[1] = $_GET['argv']; }
//$ch = curl_init(); // create curl resource 
////curl_setopt($ch, CURLOPT_URL, "http://teste.autenticacertificado.com.br/php_modules/boleto_registro/boleto_GeraRemessa.class.php");
//curl_setopt($ch, CURLOPT_URL, "https://www.autenticacertificado.com.br/mods/boleto_registro/teste.php");
////curl_setopt($ch, CURLOPT_URL, "http://viacep.com.br/ws/{$argv[1]}/json");	
//
////return the transfer as a string 
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//
//// $output contains the output string 
//$output = curl_exec($ch);
//printf($output);	
//
//// close curl resource to free up system resources 
//curl_close($ch);

function get_web_page( $url ){
    $options = array(
        CURLOPT_RETURNTRANSFER => true,     // return web page
        CURLOPT_HEADER         => false,    // don't return headers
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_ENCODING       => "",       // handle all encodings
        CURLOPT_USERAGENT      => "spider", // who am i
        CURLOPT_AUTOREFERER    => true,     // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
        CURLOPT_TIMEOUT        => 120,      // timeout on response
        CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        CURLOPT_SSL_VERIFYPEER => false     // Disabled SSL Cert checks
    );

    $ch      = curl_init( $url );
    curl_setopt_array( $ch, $options );
    $content = curl_exec( $ch );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    curl_close( $ch );

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = $content;
    return $header;
}
//$output = get_web_page('https://www.autenticacertificado.com.br/mods/boleto_registro/teste.php');
$output = get_web_page('https://www.autenticacertificado.com.br/mods/boleto_registro/boleto_GeraRemessa.class.php');
//print_r($output['content']);
//print_r($output);exit;

function saveToFile($filename="",$contentFile){
	//print "\n---".$filename."\n";
	if($filename===""){
	   $filename = "logs/".date('Ymd').".log";
	}	
	
	if(!file_exists(dirname($filename))){
		mkdir(dirname($filename), 0755, true);
		chmod(dirname($filename), 0755);
	}	
	try {
		if(($f=fopen($filename, 'w'))){
			fwrite($f, $contentFile);
		}else{
			throw new Exception("Falha ao gerar o arquivo!");
		}
		fclose($f);		
	} catch (Exception $e) {
		//echo 'Exception: ',  $e->getMessage()," - <b>File:</b>".$e->getFile()."<b> Linha:</b>".$e->getLine(),"</pre>\n";
		$logMsg = "Exception:".$e->getMessage()." - File: ".$e->getFile()." Linha: ".$e->getLine();
		echo $logMsg;
		return false;
		fclose($f);
	} 
}

$fileName = __DIR__."/remessaLogs".DIRECTORY_SEPARATOR.date("dmY-His").".SIGCB.CEF.CONV790262.REM".".LOG";
//echo "\n".$fileName."\n";
saveToFile($fileName,$output['content']);	


?>