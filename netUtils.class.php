﻿<?php

class netUtils{
    private $host	  = "localhost";
    private $ipAdress = '127.0.0.1';
    private $protcolo = "http://";    
    private $url;
	private $logs;
	private $allResult;
	private $contentResult;
    
    public function __construct($params=[]){
        foreach ($params as $key => $value) {
           $this->{$key} = $value;
        }
        $this->url = $this->protcolo.$this->host;
		
    }
	
	public function curl( $url="" ){		
		(!empty($url)) ? $this->url=$url : $this->url = $this->protcolo.$this->host;
		
		$options = array(
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_USERAGENT      => "spider", // who am i
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_SSL_VERIFYPEER => false     // Disabled SSL Cert checks
		);
		
		$ch      = curl_init( $this->url );
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );

		$header['errno']   = $err;
		$header['errmsg']  = $errmsg;
		$header['content'] = $content;
		
		$this->allResult = $header;
		$this->contentResult = $header['content'];
		
	}	
	public static function curl2( $url ){		
		if(empty($url)){ return false; }
		
		$options = array(
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_USERAGENT      => "spider", // who am i
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_SSL_VERIFYPEER => false     // Disabled SSL Cert checks
		);
		
		$ch      = curl_init( $url );
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );

		$header['errno']   = $err;
		$header['errmsg']  = $errmsg;
		$header['content'] = $content;
		
		return $header;		
	}	

	public function setHost($string){
		$this->host = $value;	
	}
	public function setProtocol($value){
		$this->protcolo = $value;	
	}
	public function setIpAdress($value){
		$this->ipAdress = $value;	
	}
	public function setUrl($value){
		$this->url = $value;	
	}
	
	public function getAllResult(){
		return $this->allResult;
	}
	public function getUrl(){
		return $this->url;
	}
	public function getContentResult(){
		return $this->contentResult;
	}


}

//$demo = new netUtils(array(
//			"host"=>"viacep.com.br/ws/70150900/json",
//			"protocolo"=>"http://"
//		));
//print_r($demo->getContentResult());

//print_r(netUtils::curl2("http://viacep.com.br/ws/70150900/json"));
	

